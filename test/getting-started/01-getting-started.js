'use strict'

const Lab = require('lab')
const Code = require('code')
const Path = require('path')
const Hapi = require('hapi')
const lab = (exports.lab = Lab.script())
const { describe, it } = lab
const experiment = lab.experiment
const test = lab.test
const expect = Code.expect


describe('inject requests with server.inject,', () => {
    it('injects a request to a hapi server without a route', async () => {
        const server = new Hapi.Server()

        // these must match the route you want to test
        const injectOptions = {
            method: 'GET',
            url: '/'
        }

        // wait for the response and the request to finish
        const response = await server.inject(injectOptions)

        // alright, set your expectations :)
        expect(response.statusCode).to.equal(404)
    })
})

describe('inject requests with server.inject,', () => {

    it('inject a request', async () => {
        const server = new Hapi.Server()
        server.route({
            method: 'GET',
            path: '/',
            handler: () => {
                return {
                    name: 'Marcus',
                    isDeveloper: true,
                    isHapiPassionate: 'YEEEEAHHH'
                }
            }
        })

        // these must match the route you want to test
        const injectOptions = {
            method: 'GET',
            url: '/'
        }

        // wait for the response and the request to finish
        const response = await server.inject(injectOptions)

        // alright, set your expectations :)
        expect(response.statusCode).to.equal(200)

        // shortcut to payload
        const payload = JSON.parse(response.payload)
        expect(payload.name).to.equal('Marcus')

        // of course you can assign more “expect” statements
    })
})