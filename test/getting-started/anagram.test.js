'use strict'

const Lab = require('lab')
const Code = require('code')
const Path = require('path')
const Hapi = require('hapi')
const lab = (exports.lab = Lab.script())
const { describe, it } = lab
const experiment = lab.experiment
const test = lab.test
const expect = Code.expect

describe('inject requests with server.inject,', () => {

    it('inject a request', async () => {
        const server = new Hapi.Server()
        server.route({
            method: 'POST',
            path: '/anagram/checking',
            handler: () => {
                return {
                   wordA: 'silent',
                   wordB: 'listen',
                }
            }
        })

        // these must match the route you want to test
        const injectOptions = {
            method: 'POST',
            url: '/anagram/checking'
        }

        // wait for the response and the request to finish
        const response = await server.inject(injectOptions)

        // alright, set your expectations :)
        expect(response.statusCode).to.equal(200)

        // shortcut to payload
        const payload = JSON.parse(response.payload)
        expect(payload.wordA).to.equal('silent')

        // of course you can assign more “expect” statements
    })
})