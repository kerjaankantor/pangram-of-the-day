const Hapi = require('@hapi/hapi');
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');
const PangramModel = require('./pangram');
const Mongoose = require("mongoose");
const Joi = require("joi");
const fs = require('fs');

function processData(pangramKey, pangramWord) {
    var letters = input.replace(/\s/g, '').toLowerCase().split('');
    var countDistinctLetters = letters.filter(function (x, pos) { return letters.indexOf(x) === pos; }).length;
    console.log(countDistinctLetters);
    if (countDistinctLetters === 26) {
        return true;
    } else {
        return false;
    }
}

function checkAnagram(wordA, wordB) {
    wordA = wordA.replace(/[^\w]/g, '').toLowerCase();
    wordB = wordB.replace(/[^\w]/g, '').toLowerCase();
    if(wordA == wordB){
        return false;
    }else if (sortingWord(wordA) == sortingWord(wordB)) {
        return true;
    }
    return false;
}

function sortingWord(word) {
    return word.split('').sort().join('');
}

function wordList() {
    var rawdata = fs.readFileSync('words_dictionary.json');
    return wordDictionary = JSON.parse(rawdata);
}

(async () => {
    const server = await new Hapi.Server({ "host": "0.0.0.0", "port": process.env.PORT || 3000 });
    Mongoose.connect("mongodb+srv://admin:admin123@cluster0-89hyy.mongodb.net/pangram?retryWrites=true&w=majority");
    const words = wordList();

    const swaggerOptions = {
        info: {
            title: 'Test API Documentation',
            version: "0.0.1",
        }
    };

    await server.register([
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: swaggerOptions
        }
    ]);

    try {
        await server.start();
        console.log('Server running at:', server.info.uri);
    } catch (err) {
        console.log(err);
    }

    server.route({
        method: 'GET',
        path: '/',
        options: {
            description: 'Pangram App test connection',
            notes: 'Return Some data dummy',
            tags: ['api'],
            handler: (request, h) => {

                return 'Hello World!';
            }
        },
    });

    server.route({
        method: "GET",
        path: "/test/{username}",
        options: {
            description: 'query with mock data',
            notes: 'input with nraboy',
            tags: ['api'],
            validate: {
                params: Joi.object({
                    username: Joi.string().required().description('put user name'),
                })
            },
            handler: (request, h) => {
                var accountMock = {};
                if (request.params.username == "nraboy") {
                    accountMock = {
                        "username": "nraboy",
                        "password": "1234",
                        "twitter": "@nraboy",
                        "website": "https://www.google.com"
                    }
                }
                return accountMock;
            }
        },
    });

    server.route({
        method: "GET",
        path: "/pangram",
        options: {
            description: 'get pangram',
            notes: 'will be random ',
            tags: ['api'],
            handler: async (request, h) => {
                try {
                    var pangram = await PangramModel.find();
                    console.log(pangram);
                    return h.response(pangram);
                } catch (error) {
                    console.log(error);
                    return h.response(error).code(500);
                }
            }
        },
    });

    server.route({
        method: "POST",
        path: "/pangram/save",
        options: {
            description: 'save pangram',
            notes: 'saving pangram by API',
            tags: ['api'],
            // validate: {
            //     payload: {
            //         pangramKey : Joi.string().required(),
            //         pangramWord : Joi.string().required(),
            //     }
            // },   
            handler: async (request, h) => {
                try {
                    var ObjectId = require('mongodb').ObjectID
                    console.log(request.payload);
                    request.payload._id = new ObjectId();
                    await PangramModel.create(request.payload, function (err, result) {
                        if (err) {
                            console.log(err);
                            return console.error(err);
                        } else {
                            return Promise.resolve(request.payload.status = "");
                        }
                    });
                } catch (error) {
                    console.log(error);
                    return h.response(error).code(500);
                }
            }
        },
    });

    server.route({
        method: "POST",
        path: "/anagram/checking",
        options: {
            description: 'for checking two words anagram',
            notes: 'will be random ',
            tags: ['api'],
            handler: async (request, h) => {
                try {
                    var isAnagram = false;
                    if (checkAnagram(request.payload.wordA, request.payload.wordB)) {
                        console.log("true");
                        // console.log(words);
                        console.log(words[request.payload.wordA]);
                        console.log(words[request.payload.wordB]);
                        if(words[request.payload.wordA]!= null 
                            && words[request.payload.wordB] != null){
                                isAnagram = true;
                        }
                    }
                    request.payload.anagram =  isAnagram;
                    return h.response(request.payload);
                } catch (error) {
                    console.log(error);
                    return h.response(error).code(500);
                }
            }
        },
    });

    server.route({
        method: "GET",
        path: "/anagram/random",
        options: {
            description: 'get word randomly from json',
            notes: 'will be random ',
            tags: ['api'],
            handler: async (request, h) => {
                try {
                    var dataLength = Object.keys(words).length;
                    var index =Math.floor(Math.random() * dataLength);
                    var random =Object.keys(words)[index];
                    console.log(random);
                    return h.response(random);
                } catch (error) {
                    console.log(error);
                    return h.response(error).code(500);
                }
            }
        },
    });
})();