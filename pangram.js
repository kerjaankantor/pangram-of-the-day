const Mongoose = require("mongoose");

const PangramSchema = new Mongoose.Schema(
    {
        _id:{
            type:String
        },
        pangramKey:{
            type:String
        },
        pangramWord:{
            type:String
        }
    }
);

const Pangram = Mongoose.model("Pangram", PangramSchema);
module.exports = Pangram;